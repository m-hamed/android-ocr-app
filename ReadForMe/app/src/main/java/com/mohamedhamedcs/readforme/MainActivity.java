package com.mohamedhamedcs.readforme;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import com.edmodo.cropper.CropImageView;
import com.googlecode.tesseract.android.TessBaseAPI;
import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class MainActivity extends ActionBarActivity {
    private static final int ACTION_TAKE_PHOTO = 1;
    private static final String BITMAP_STORAGE_KEY = "viewbitmap";
    private static final String IMAGEVIEW_VISIBILITY_STORAGE_KEY = "imageviewvisibility";
    Bitmap croppedImage;
    private CropImageView cropImageView;
    private TextView textViewResultOCR;
    //Crop & Rotate Buttons
    private LinearLayout cropNrotate ;
    private LinearLayout sliders;
    private Button cropButton;
    private Button rotateButton;
    private Bitmap mImageBitmap;
    private Button doneButton;
    private Boolean OPENCVLOADED = false ;
    private String mCurrentPhotoPath;
    private File AppDirectory = null ;

    private String  AppName = "ReadForMe" ;
    //Get -or Create - directory  path for Saving Picture ///storage/emulated/0/AppName/
    private File getAlbumDir() {
        String DirString = Environment.getExternalStorageDirectory().toString() +"/"+AppName;
        AppDirectory = new File(DirString);
        boolean success = true;
        if (!AppDirectory.exists()) {
            //Create the Directory
            success = AppDirectory.mkdir();
        }
        if (success) {
            Log.wtf(AppName, DirString + " Created Successfully ");
        } else {
            Toast.makeText(MainActivity.this, "Failed To create App Directory", Toast.LENGTH_LONG).show();
        }
        return AppDirectory;
    }

    private void dispatchTakePictureIntent(int actionCode) {

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        switch(actionCode) {
            case ACTION_TAKE_PHOTO :
                File image = null ;
                try {
                    image = File.createTempFile(AppName, ".jpg", getAlbumDir());
                    mCurrentPhotoPath = image.getAbsolutePath();
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(image));
                } catch (IOException e) {
                    e.printStackTrace();
                    image = null;
                    mCurrentPhotoPath = null;
                }
                break;

            default:
                break;
        } // switch

        startActivityForResult(takePictureIntent, actionCode);
    }


    Button.OnClickListener mTakePicOnClickListener =
            new Button.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (OPENCVLOADED )
                    {
                        sliders.setVisibility(View.GONE);
                        dispatchTakePictureIntent(ACTION_TAKE_PHOTO);
                    }
                }
            };
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Crop Image View
        cropImageView = (CropImageView) findViewById(R.id.CropImageView);
        cropImageView.setVisibility(View.INVISIBLE);

        cropNrotate = (LinearLayout) findViewById(R.id.cropNrotate);
        cropNrotate.setVisibility(View.GONE);
        sliders = (LinearLayout) findViewById(R.id.sliders);
        sliders.setVisibility(View.GONE);
        textViewResultOCR = (TextView) findViewById(R.id.textViewResultOCR);
        final TextView textViewDilationValue = (TextView) findViewById(R.id.textViewDilationValue);
        final TextView textViewErosionValue = (TextView) findViewById(R.id.textViewErosionValue);
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_5, this, new BaseLoaderCallback(this) {
            @Override
            public void onManagerConnected(int status) {
                super.onManagerConnected(status);
                if (status == BaseLoaderCallback.SUCCESS) {
                    OPENCVLOADED = true;
                    sliders.setVisibility(View.GONE);
                    dispatchTakePictureIntent(ACTION_TAKE_PHOTO);
                }

            }
        });

        final SeekBar seekbarErosion = (SeekBar) findViewById(R.id.seekBarErosion);
        final SeekBar seekbarDilation = (SeekBar) findViewById(R.id.seekBarDilation);

        SeekBar.OnSeekBarChangeListener onSeekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (seekBar.getId() == R.id.seekBarDilation)
                    textViewDilationValue.setText("" + seekBar.getProgress());
                if (seekBar.getId() == R.id.seekBarErosion)
                    textViewErosionValue.setText("" + seekBar.getProgress());
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (croppedImage == null) return ;
                Bitmap preparedImage = prepareToOCR(croppedImage, seekbarDilation.getProgress(), seekbarErosion.getProgress());
                cropImageView.setImageBitmap(preparedImage);
                try {
                    String ocrResult = processOCR(preparedImage);
                    textViewResultOCR.setText(ocrResult);
                } catch (Exception ex) {
                    Log.wtf("OnStopTracking",ex.getMessage());
                }
            }
        };
        seekbarDilation.setOnSeekBarChangeListener(onSeekBarChangeListener);
        seekbarErosion.setOnSeekBarChangeListener(onSeekBarChangeListener);

       doneButton = (Button) findViewById(R.id.doneButton);
        doneButton.setVisibility(View.GONE);
        doneButton.setOnClickListener(new  View.OnClickListener()
        {
            @Override
            public void onClick(View v) {

                Log.w("donButton","Click");
            }
        });
        rotateButton = (Button) findViewById(R.id.Button_rotate);
        rotateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cropImageView.rotateImage(90);
            }
        });
        cropButton = (Button) findViewById(R.id.Button_crop);
        cropButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                croppedImage = cropImageView.getCroppedImage();
                cropImageView.setVisibility(View.VISIBLE);
                cropImageView.setVisibility(View.VISIBLE);
                cropNrotate.setVisibility(View.GONE);
                sliders.setVisibility(View.VISIBLE);
                Bitmap preparedImage = prepareToOCR(croppedImage, seekbarDilation.getProgress(), seekbarErosion.getProgress());
                cropImageView.setImageBitmap(preparedImage);
                try {
                    String ocrResult = processOCR(preparedImage);
                    textViewResultOCR.setText(ocrResult);
                } catch (Exception ex) {
                    Toast.makeText(MainActivity.this, ex.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });
        mImageBitmap = null;
        Button picBtn = (Button) findViewById(R.id.picButton);
        setBtnListenerOrDisable(
                picBtn,
                mTakePicOnClickListener,
                MediaStore.ACTION_IMAGE_CAPTURE
        );
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case ACTION_TAKE_PHOTO : {
                if (resultCode == RESULT_OK) {
                    if (mCurrentPhotoPath != null) {
                        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath);
                        Log.wtf("ImagePath",mCurrentPhotoPath);
                        cropImageView.setImageBitmap(bitmap);
                        cropImageView.setVisibility(View.VISIBLE);
                        cropNrotate.setVisibility(View.VISIBLE);
                        doneButton.setVisibility(View.VISIBLE);
                        mCurrentPhotoPath = null;
                    }
                }
                break;
            } // ACTION_TAKE_PHOTO
        } // switch
    }

    // Some lifecycle callbacks so that the image can survive orientation change
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(BITMAP_STORAGE_KEY, mImageBitmap);
        outState.putBoolean(IMAGEVIEW_VISIBILITY_STORAGE_KEY, (mImageBitmap != null) );
        super.onSaveInstanceState(outState);
    }
    /// OCR
       private String processOCR(Bitmap preparedImage) {
//TODO init Tess Only One Time Makes it s much faster
        String recognizedText = "";
        try
        {

            TessBaseAPI baseApi = new TessBaseAPI();
            String DATA_PATH = Environment.getExternalStorageDirectory().toString() + "";
            //baseApi.init(DATA_PATH, "ArabicNumbers");
            baseApi.init(DATA_PATH, "eng");
            baseApi.setImage(preparedImage);
            recognizedText = baseApi.getUTF8Text();
            baseApi.end();
            //In case Of Arabic Numbers Only
            /*
            recognizedText = recognizedText.replace("A", "0");
            recognizedText = recognizedText.replace("B", "1");
            recognizedText = recognizedText.replace("C", "2");
            recognizedText = recognizedText.replace("D", "3");
            recognizedText = recognizedText.replace("E", "4");
            recognizedText = recognizedText.replace("F", "5");
            recognizedText = recognizedText.replace("G", "6");
            recognizedText = recognizedText.replace("H", "7");
            recognizedText = recognizedText.replace("I", "8");
            recognizedText = recognizedText.replace("J", "9");
            */
        }
        catch (Exception ex)
        {
            Log.wtf("Tesseract ", ex.toString());
        }


        return recognizedText;
        //return "tess-two not here, wait for it";
    }

    // preprocessing .. apply threshloding , dilation and erosion
    private Bitmap prepareToOCR(Bitmap croppedImage, int dilation_size, int erosion_size) {
        //docs.opencv.org/trunk/doc/py_tutorials/py_imgproc/py_thresholding/py_thresholding.html
        Bitmap imageCopy = croppedImage.copy(croppedImage.getConfig(), false);
        Mat sourceMat = new Mat();
        Utils.bitmapToMat(imageCopy, sourceMat);

        // structure element used in erosion & dilation
        Mat elementEosion = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new org.opencv.core.Size(2 * erosion_size + 1, 2 * erosion_size + 1));
        Mat elementDilation = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new org.opencv.core.Size(2 * dilation_size + 1, 2 * dilation_size + 1));
        //thresholding
        Mat grayMat = new Mat();
        grayMat = sourceMat;
        Mat threshlodMat = new Mat();
        threshlodMat = sourceMat;
        //convert to gray
        Imgproc.cvtColor(sourceMat, grayMat, Imgproc.COLOR_RGB2GRAY);
        //Manual threshloding -- Bad Way
        //Imgproc.threshold(grayMat, threshlodMat, minThreshold, 255, Imgproc.THRESH_BINARY);
        //Adaptive Thresholding -- Amazing Way
       //Imgproc.adaptiveThreshold(grayMat,threshlodMat,255,Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C, Imgproc.THRESH_BINARY,11,2);

        // Otsu's thresholding after Gaussian filtering
        Imgproc.GaussianBlur(grayMat,grayMat,new Size(5,5),0);
        Imgproc.threshold(grayMat,threshlodMat,0,255,Imgproc.THRESH_BINARY+Imgproc.THRESH_OTSU);
        //erosion
        Imgproc.erode(threshlodMat, threshlodMat, elementEosion);
        //dilation
        Imgproc.dilate(threshlodMat, threshlodMat, elementDilation);
        Utils.matToBitmap(threshlodMat, imageCopy);

        return imageCopy;
    }



    /**
     * Indicates whether the specified action can be used as an intent. This
     * method queries the package manager for installed packages that can
     * respond to an intent with the specified action. If no suitable package is
     * found, this method returns false.
     * http://android-developers.blogspot.com/2009/01/can-i-use-this-intent.html
     *
     * @param context The application's environment.
     * @param action The Intent action to check for availability.
     *
     * @return True if an Intent with the specified action can be sent and
     *         responded to, false otherwise.
     */
    public static boolean isIntentAvailable(Context context, String action) {
        final PackageManager packageManager = context.getPackageManager();
        final Intent intent = new Intent(action);
        List<ResolveInfo> list =
                packageManager.queryIntentActivities(intent,
                        PackageManager.MATCH_DEFAULT_ONLY);
        return list.size() > 0;
    }

    private void setBtnListenerOrDisable(
            Button btn,
            Button.OnClickListener onClickListener,
            String intentName
    ) {
        if (isIntentAvailable(this, intentName)) {
            btn.setOnClickListener(onClickListener);
        } else {
            btn.setText("No Camera ! ");
            btn.setClickable(false);
        }
    }

}